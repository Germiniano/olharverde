import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Header1Component } from './components/layouts/header1/header1.component';
import { PreloaderComponent } from './components/layouts/preloader/preloader.component';
import { Footer1Component } from './components/layouts/footer1/footer1.component';
import { Homepage1Component } from './components/pages/homepage1/homepage1.component';
import { TeamComponent } from './components/pages/team/team.component';
import { DepositionsComponent } from './components/pages/depositions/depositions.component';
import { OurMissionComponent } from './components/pages/our-mission/our-mission.component';
import { WhoWeAreComponent } from './components/pages/who-we-are/who-we-are.component';
import { EventsComponent } from './components/pages/events/events.component';
import { RegisterComponent } from './components/pages/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    Header1Component,
    PreloaderComponent,
    Footer1Component,
    Homepage1Component,
    TeamComponent,
    DepositionsComponent,
    OurMissionComponent,    
    WhoWeAreComponent, 
    EventsComponent, 
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
