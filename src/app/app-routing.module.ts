import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Homepage1Component } from './components/pages/homepage1/homepage1.component';
import { DepositionsComponent } from './components/pages/depositions/depositions.component';
import { OurMissionComponent } from './components/pages/our-mission/our-mission.component';
import { TeamComponent } from './components/pages/team/team.component';
import { WhoWeAreComponent } from './components/pages/who-we-are/who-we-are.component';
import { EventsComponent } from './components/pages/events/events.component';
import { RegisterComponent } from './components/pages/register/register.component';

const routes: Routes = [

{path: '', component: Homepage1Component},
{path: 'depoimentos', component: DepositionsComponent},
{path: 'nossa-missao', component: OurMissionComponent},
{path: 'nossa-equipe', component: TeamComponent},
{path: 'quem-somos', component: WhoWeAreComponent},
{path: 'eventos', component: EventsComponent},
{path: 'cadastro', component: RegisterComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
