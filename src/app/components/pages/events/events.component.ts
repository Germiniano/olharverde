import { ConstantPool } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  teste() {
    var i = 0;
    var j = 0;
    var trab = 4;
    var varil = 7;

    while (i < 13) {
      varil = varil + 10;
      i = i + 5;

      while (j < 12) {
        if ((j%2) == 0) {
          trab = trab + 5;
        }
        j += 2;
      }
    }
    console.log('Trab: ' + trab);
    console.log('Var: ' + varil);
  }

}
