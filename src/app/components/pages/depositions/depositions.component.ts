import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

declare let videojs: any;

@Component({
  selector: 'app-depositions',
  templateUrl: './depositions.component.html',
  styleUrls: ['./depositions.component.css']
})
export class DepositionsComponent implements OnInit {
  
  @ViewChild('myvid') vid: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    const options = {
      controls: true,
      autoplay: false,
      preload: 'auto',
      techOrder: ['html5']
    };

    
  }

}
